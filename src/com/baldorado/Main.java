package com.baldorado;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner user = new Scanner(System.in);
        System.out.println("Enter year: ");
        Integer year = user.nextInt();
        System.out.println("Year is " + year);
        boolean leap = false;

        if (year % 4 == 0) {
            if (year % 100 == 0) {
                if (year % 400 == 0)
                    leap = true;
                else
                    leap = false;
        } else
            leap = true;
        }
        else {
            leap = false;
        }
        if (leap == true)
            System.out.println("The year entered is Leap Year");
        else
            System.out.println("The year is not Leap Year");
    }
}
